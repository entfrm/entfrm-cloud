package com.entfrm.biz.devtool;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
* @author entfrm
*/
@SpringCloudApplication
public class DevtoolApplication {
	public static void main(String[] args) {
		SpringApplication.run(DevtoolApplication.class, args);
	}
}
