package com.entfrm.auth.service;

import com.entfrm.core.base.api.R;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码接口
 *
 * @author entfrm
 * @create 2020-6-10
 */
public interface ValidateCodeService {
    /**
     * 生成验证码
     * @param deviceId 前端唯一标识
     */
    R genCode(String deviceId);

    /**
     * 获取验证码
     * @param deviceId 前端唯一标识/手机号
     */
    String getCode(String deviceId);

    /**
     * 删除验证码
     * @param deviceId 前端唯一标识/手机号
     */
    void remove(String deviceId);

    /**
     * 验证验证码
     */
    void validate(HttpServletRequest request);

    /**
     * 发送短信验证码
     * @param phone 手机号
     */
    R sendSmsCode(String phone);

    /**
     * 发送邮件验证码
     * @param email 电子邮箱
     */
    R sendEmailCode(String email);
}
