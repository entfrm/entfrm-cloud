package com.entfrm.auth.controller;


import com.entfrm.auth.service.ValidateCodeService;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.constant.SecurityConstants;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 验证码提供
 * @author entfrm
 * @date 2018/12/18
 */
@RestController
public class ValidateCodeController {

    @Autowired
    private ValidateCodeService validateCodeService;

    /**
     * 创建验证码
     *
     * @throws Exception
     */
    @GetMapping("/validata/code/{deviceId}")
    public void createCode(@PathVariable String deviceId, HttpServletResponse response) throws Exception {
        Assert.notNull(deviceId, "机器码不能为空");
        /*// 设置请求头为输出图片类型
        CaptchaUtil.setHeader(response);
        // 三个参数分别为宽、高、位数
        GifCaptcha gifCaptcha = new GifCaptcha(100, 35, 4);
        // 设置类型：字母数字混合
        gifCaptcha.setCharType(Captcha.TYPE_DEFAULT);
        // 保存验证码
        validateCodeService.saveImageCode(deviceId, gifCaptcha.text().toLowerCase());
        // 输出图片流
        gifCaptcha.out(response.getOutputStream());*/
    }

    /**
     * 发送手机验证码
     * 后期要加接口限制
     *
     * @param phone 手机号
     * @return R
     */
    @ResponseBody
    @GetMapping("/validata/smsCode/{phone}")
    public R createCode(@PathVariable String phone) {
        Assert.notNull(phone, "手机号不能为空");
        return validateCodeService.sendSmsCode(phone);
    }
}
